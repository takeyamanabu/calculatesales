package jp.alhinc.takeya_manabu.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
public class CalculateSales {
	public static void main(String[] args) {
		//コマンドライン引数のエラー確認
		if(args.length != 1){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		//支店定義ファイルが存在しない場合エラーを表示し、システムを終了する処理
		File BranchListFile = new File(args[0], "branch.lst");
		if(!BranchListFile.exists()){
			System.out.println("支店定義ファイルが存在しません");
			return;
		}
		// 支店名・支店コードの箱の宣言
		Map<String, String> branchNameMap = new HashMap<String, String>();
		// コード・合計金額の箱の宣言
		Map<String, Long> salesSumMap = new HashMap<String, Long>();
		//支店定義ファイルの読込メソッドを呼び出す
		String dirStr = args[0];
		//Falseを戻り値として受け取った場合、システムを終了する
		if(!readCodeMethod(dirStr,"branch.lst","支店","[0-9]{3}",branchNameMap,salesSumMap)){
			return;
		}
		//売上ファイルを読み込む前に、売上ファイルのみをリストアップする工程
		// 支店定義ファイルや売上ファイルの存在するディレクトリの宣言
		File dir = new File(args[0]);
		//dir内にあるファイルの一覧をFile型の配列で返す
		File[] files = dir.listFiles();
		// ディレクトリ内の売上ファイルのみを保管するリストの宣言
		ArrayList<File> salesList = new ArrayList<File>();
		for(int i = 0; i<files.length; i++) {
			//ディレクトリ内の一覧からファイルを１つ指定しファイル名を返す
			String fileName =files[i].getName();
			// 条件のファイルかどうかの真偽(ファイル名の一致とフォルダの除外）
			if(fileName.matches("\\d{8}\\.rcd$")&& files[i].isFile() ) {
				//リストに売上ファイルのみ追加する
				salesList.add(files[i]);
			}
		}
		//売上ファイルが連番かどうか確認する
		int sizeOfList = salesList.size();
			for(int i = 0; i<sizeOfList-1; i++) {
				int codeNum =  Integer.parseInt(salesList.get(i).getName().substring(0, 8));
				int nextCodeNum = Integer.parseInt(salesList.get(i+1).getName().substring(0, 8));
				if(nextCodeNum-codeNum!=1){
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
			}
			//空のBufferedReaderをつくる
			BufferedReader br = null;
		//売上ファイルを読み込み、支店名ごとに合計金額を加算する処理
		try {
			for(int i = 0; i<sizeOfList; i++) {
				//支店コードと売上金額を一時的に保管するリスト
				ArrayList<String> CodeAndSum = new ArrayList<String>();
				//リストのファイルを１つ参照
				File earning = salesList.get(i);
				String line;
				br = new BufferedReader(new FileReader(earning));
				//売上ファイル内の文字列を最後まで読み込む
				while ((line = br.readLine()) != null){
					//支店名、金額をリストに一時保管
					CodeAndSum.add(line);
				}
				//売上ファイルの要素数をチェックできるようにする（売上ファイル内の行数を確認）
				int NumOfElement = CodeAndSum.size();
				String fileName = earning.getName();
				//売上ファイルの要素数が２ではない場合
				if(NumOfElement!=2){
					System.out.println(fileName+"のフォーマットが不正です");
					return;
				}
				//売上ファイルの支店コードがbranchNameMapに存在するかにより、エラー処理実装
				//リストのはじめの要素(支店コード）を参照する
				String key = CodeAndSum.get(0);
				if(!branchNameMap.containsKey(key)){
					System.out.println(fileName + "の支店コードが不正です");
					return;
				}
				//リストの二番目の要素（売上金額）を参照する
				String strFileSum = CodeAndSum.get(1);
				//売上金額が数字かどうか確認する処理
				if(!strFileSum.matches("^[0-9]*$")){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				long fileSum = new Long(strFileSum);
				//支店コードをもとに支店の合計金額を呼び出す
				long mapSum =salesSumMap.get(key);
				//支店に加算する金額が１０桁を超えていないかの確認
				int fileNumOfDigits = String.valueOf(fileSum).length();
				if(fileNumOfDigits>10){
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				// salesSumMapから取得した合計金額 ＋ 売上ファイルから合計金額
				mapSum = mapSum + fileSum;
				int mapNumOfDigits = String.valueOf(mapSum).length();
				//支店の合計金額が１０桁を超えていないかの確認
				if(mapNumOfDigits>10){
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				// salesSumMapに合計金額を代入する
				salesSumMap.put(key, mapSum);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}
		//ファイル出力メソッドを呼び出す
		if(!fileOutMethod(dirStr,"branch.out",branchNameMap ,salesSumMap)){
			return;
		}
	}



	//支店定義ファイルの読込機能のメソッド宣言を行う
	public static boolean readCodeMethod(String dirStr, String fileName, String category, String format,Map<String, String> mapName,Map<String, Long> mapSales){
		BufferedReader br = null;
		//支店定義ファイルを読み込み、支店コード及び支店名を紐付けする処理
		try {
			File file = new File(dirStr, fileName);
			String line;
			br = new BufferedReader(new FileReader(file));
			while ((line = br.readLine()) != null) {
				//１行読む際、その文字列をドットを境に二つの要素に分割する
				String[] lines = line.split(",");
				String branchCode = new String(lines[0]);
				//支店コードが不正な場合エラー表示し、システムを終了する処理の実装
				if(!branchCode.matches(format)){
					System.out.println(category +"定義ファイルのフォーマットが不正です");
					return false;
					//１行のカンマで区切られた要素数が２ではない場合、エラーを表示する
				} else if(lines.length!=2){
					System.out.println(category +"定義ファイルのフォーマットが不正です");
					return false;
				}
				//支店コードと名前を支店名を入れるマップに追加する
				mapName.put(lines[0], lines[1]);
				//支店コードを売上を入れるマップにキーとして追加する
				mapSales.put(lines[0], (long) 0 );
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		} return true;
	}



	//ファイル出力機能のメソッド宣言を行う
	public static boolean fileOutMethod(String dirStr, String fileName, Map<String, String> mapName,Map<String, Long> mapSales){
		BufferedWriter bw = null;
		try {
			File file = new File(dirStr, fileName);
			FileOutputStream fos = new FileOutputStream(file);
			OutputStreamWriter osw = new OutputStreamWriter(fos,"UTF-8");
			 bw = new BufferedWriter(osw);
			PrintWriter pw = new PrintWriter(bw);
			//MapのentrySetメソッドはマップに含まれるキーと値のSetを返す
			for(Map.Entry<String,String> entry : mapName.entrySet() ) {
				String key = entry.getKey();
				Long mapSum = mapSales.get(key);
				//支店コード＋支店名＋合計金額
				pw.println(entry.getKey() + ","+ entry.getValue() + "," + mapSum);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(bw != null){
				try{
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}

